package reversi;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.awt.Rectangle;

/**
 * this class is used to create each individual square on the 
 * game board 
 * @author Germaine
 *
 */
public class Square{
	// declare variables
	private final int gap = 2;
	private int row;
	private int column;
	private int xLocation;
	private int yLocation;
	private final int size = 50;
	private Color color = Color.gray;
	private State state;
	private Valid valid;
	
	/**
	 * this is the constructor for the squares it sets each squares 
	 * location and accounts for the space between squares
	 */
	public Square(int row, int column, State state, Valid valid){
		this.row = row;
		this.column = column;
		this.state = state;
		this.valid = valid;
		xLocation = (size*column)+(gap*(column+1));
		yLocation = (size*row)+(gap*(row+1));
	}
	/**
	 * this method determines the points within each square
	 */
	public boolean contains(int x, int y){
		int a = xLocation;
		int b = xLocation+size;
		int c = yLocation+45;
		int d = yLocation+size+45;
		 return (a<=x && x<=b && c<=y && y<=d); 
	} 
	/**
	 * this method return the xLocation
	 */
	public int getXLocation(){
		return xLocation;
	}
	/**
	 * this method return the yLocation
	 */
	public int getYLocation(){
		return yLocation;
	}
	/**
	 * this method return the row
	 */
	public int getRow(){
		return row;
	}
	/**
	 * this method return the column
	 */
	public int getColumn(){
		return column;
	}
	/**
	 * this method returns the index of the square
	 */
	public int getIndex(){
		return (row*8)+column;
	}
	/**
	 * this method changes the state of the square
	 */
	public void changeState(){
		state = State.OCCUPIED;
	}
	/**
	 * this method returns the state of the square
	 */
	public State getState(){
		return state;
	}
	/**
	 * this method changes the color of the box if it is a valid move
	 */
	public void changeColor(Color color){
		this.color = color;
	}
	/**
	 * this method changes the squares validity
	 */
	public void changeValidity(){
		if(valid==Valid.NOTVALID){
			valid=Valid.VALID;
		}
		if (valid==Valid.VALID){
			valid=Valid.NOTVALID;
		}
	}
	/**
	 * this method draws the squares
	 */
	public void draw(Graphics2D g2){
		g2.setPaint(color);
		g2.fill(new Rectangle2D.Double(xLocation,yLocation,size,size));
	}
}