package reversi;

import java.util.ArrayList;

/**
 * this class holds an array of disks that need to be flipped along with
 * the square that caused those disks to be flipped
 * @author Germaine
 *
 */
public class Move {
	//variables
	int flipAmount;
	int index;
	/**
	 * this constructor initializes variables
	 */
	public Move(int flipAmount, int index){
		this.flipAmount = flipAmount;
		this.index = index;
		
	}
	/**
	 * this method returns the number of disks that would flip if
	 * the computer clicked on this square
	 * @return
	 */
	public int getFlipAmount(){
		return flipAmount;
	}
	/**
	 * this method returns the index in reference to legalComputerMoves
	 * @return
	 */
	public int getIndex(){
		return index;
	}
}