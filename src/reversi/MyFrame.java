package reversi;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

/**
 * this is the enum that will be switched so the program knows whose
 * turn it is, and has a color associated with it
 * @author Germaine
 *
 */
enum Turn{PERSON(Color.white,"White"), COMPUTER(Color.black,"Black");
	private Color color;
	private String str;
	Turn(Color color, String str){
		this.color=color;
		this.str=str;
	}
	Color getColor(){
		return color;
	}
	String getString(){
		return str;
	}
}

/**
 * This class creates the frame for the game and adds all of the 
 * action listeners 
 * @author Germaine
 *
 */
public class MyFrame extends JFrame {
	// declare variables
	public Timer t = new Timer(2500, new TimerListener());
	private MyPanel panel;
	final JPanel displayPanel;
	final JLabel textDisplay;
	final JTextField myText;
	private Square squareOn;
	private Turn turn;
	boolean clicked = false;
	ArrayList<Move> moves = new ArrayList<>();
	ArrayList<Square> cornerMoves = new ArrayList<>();
	ArrayList<Square> legalComputerMoves = new ArrayList<>();
	ArrayList<Disk> disksToFlip = new ArrayList<>();
	ArrayList<Disk> wouldFlip = new ArrayList<>();
	int whiteCount=2;
	int blackCount=2;
	/**
	 * this is the constructor for the MyFrame class
	 */
	public MyFrame(){
		// set frame size & location
		setSize(424,500);
		setLocation(500,100);
		
		//add the panel
		panel = new MyPanel();
		add(panel);
		turn = Turn.PERSON;
		// add the menu bar
		JMenuBar bar = new JMenuBar();
		setJMenuBar(bar);
		JMenu game = new JMenu("Game");
		JMenuItem newGame = new JMenuItem("New Game");
		JMenuItem exit = new JMenuItem("Exit");
		game.add(newGame);
		game.add(exit);
		bar.add(game);		
		newGame.addActionListener(new newGameListener());
		exit.addActionListener(new exitListener());
		// add panel
		displayPanel = new JPanel();
		panel.setBackground(Color.black);
		displayPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		// add labels
		myText = new JTextField(8);
		myText.setFocusable(false);
		displayPanel.add(myText);
		textDisplay = new JLabel("          Black:  " + blackCount + "    White: " + whiteCount);
		displayPanel.add(textDisplay);
		add(displayPanel, BorderLayout.SOUTH);
		// mouse listener
		addMouseMotionListener(new MouseAdapter(){
			/**
			 * this method highlights valid moves
			 */
			public void mouseMoved(MouseEvent e){
				clicked = false;
				for(int r=0; r<8; r++){
					for(int c=0; c<8; c++){
						if(panel.squares[r][c].contains(e.getX(),e.getY())){
							squareOn=panel.squares[r][c];
							//if valid move
							if(isValidMove(squareOn)==true){
								repaintSquares();
								panel.squares[r][c].changeColor(Color.green);
								repaint();								
							}
							else { //if (isValidMove(turn, squareOn)==false){
								repaintSquares();
								panel.squares[r][c].changeColor(Color.gray);
								repaint();
							   }
				}
			}}}});
			
		addMouseListener(new MouseAdapter(){
			/**
			 * this method adds a disk to the clicked square if it is a valid move
			 */
			public void mouseClicked(MouseEvent e){
				clicked = true;
				for(int r=0; r<8; r++){
					for(int c=0; c<8; c++){
						if(panel.squares[r][c].contains(e.getX(), e.getY()) && panel.squares[r][c].getState()==State.EMPTY){
							squareOn = panel.squares[r][c];
							if(isValidMove(squareOn)==true){
								panel.addDisk(squareOn.getRow(),squareOn.getColumn(), getTurn());
								//changeTurn(turn);
								squareOn.changeState();
								flip(Turn.PERSON);
								repaint();
								countDisks();
								textDisplay.setText("          Black:  " + blackCount + "    White: " + whiteCount);
								t.start();
							}}}}}});
		}
	/**
	 * this method paints the necessary disks after a computer move
	 */
	public void computersMove(){
		if (turn==Turn.COMPUTER){
			if(hasValidMove(turn)==false){
				changeTurn(turn);
			}
			if(hasValidMove(turn)==true){ // computer does have a valid move
				clicked=true;
				makeAComputerMove(legalComputerMoves);
				legalComputerMoves=new ArrayList<>();
				repaint();
			}}}
	/**
	 * this method determines if the player has any valid moves
	 */
	public  boolean hasValidMove(Turn turn){
		moves=new ArrayList<>();
		int index =0;
		for (int r=0; r<8; r++){
			for(int c=0; c<8; c++){
				if(turn==Turn.COMPUTER){
					if(isValidMove(panel.squares[r][c])==true){
						legalComputerMoves.add(panel.squares[r][c]);
						moves.add(new Move(disksToFlip.size(),index));
						disksToFlip=new ArrayList<>();
						index++;
					}}}}
		if(legalComputerMoves.size()!=0){
			return true; //at least one valid move was found
		}
			return false; //no valid moves were found
	}
	
	/**
	 * this method makes one of the computers valid moves
	 */
	public void makeAComputerMove(ArrayList<Square> legalComputerMoves){
		if(isValidMove(panel.squares[0][0])==true){
			squareOn=panel.squares[0][0];
		}
		else if(isValidMove(panel.squares[0][7])==true){
			squareOn=panel.squares[0][7];
		}
		else if(isValidMove(panel.squares[7][0])==true){
			squareOn=panel.squares[7][0];
		}
		else if(isValidMove(panel.squares[7][7])==true){
			squareOn=panel.squares[7][7];
		}
		else{
			int r = (int) (Math.random()*legalComputerMoves.size());
			//System.out.println("Legal Computer Moves size:" + legalComputerMoves.size());
			//System.out.println("Moves size:" + moves.size());
			//squareOn=flipFewest(); //assigns the next computer move to be the square
									//that flips the fewest disks of the user
			squareOn = legalComputerMoves.get(r);
		}
		clicked=true;
		isValidMove(squareOn);
		panel.addDisk(squareOn.getRow(),squareOn.getColumn(), turn);
		changeTurn(turn);
		squareOn.changeState();
		}
	/**
	 * this method determines which of the valid moves will flip the fewest disks
	 */
	public Square flipFewest(){
		clicked = false;
		int fewestIndex = moves.get(0).getIndex();
		int fewestToFlip = moves.get(0).getFlipAmount();
		for(int i=1; i<moves.size(); i++){ //legalComputerMoves.size() should be interchangeable with moves.size()
			clicked = false;
			if(moves.get(i).getFlipAmount()<fewestToFlip && moves.get(i).getFlipAmount()>0){
				fewestToFlip = moves.get(i).getFlipAmount();
				fewestIndex = moves.get(i).getIndex();
				System.out.println("Fewest: " + fewestToFlip);
			}
		}
		return legalComputerMoves.get(fewestIndex);
	}
	
	/**
	 * this method determines if a clicked square is occupied
	 */
	public boolean squareEmpty(Square square){
		if (square.getState()==State.OCCUPIED){
			return false;
		}
		return true;
	}
	
	/**
	 * this method changes whose turn it is
	 */
	public Turn changeTurn(Turn turn){
		disksToFlip=new ArrayList<>();
		countDisks();
		textDisplay.setText("          Black:  " + blackCount + "    White: " + whiteCount);
		if (turn==Turn.COMPUTER){
			this.turn=Turn.PERSON;
			return Turn.PERSON;
		}
		if (turn==Turn.PERSON){
			this.turn=Turn.COMPUTER;
			clicked=false;
			computersMove();
			return Turn.COMPUTER;
		}
		return turn;
	}
	/**
	 * this method counts the number of disk for black and white and determines
	 * if there is a winner and displays the message in the text field
	 */
	public void countDisks(){
		whiteCount=0;
		blackCount=0;
		for(int r=0; r<8; r++){
			for(int c=0; c<8; c++){
				if(panel.disks[r][c]!=null && panel.disks[r][c].getColor()==Color.black){
					blackCount++;
				}
				if(panel.disks[r][c]!=null && panel.disks[r][c].getColor()==Color.white){
					whiteCount++;
				}
			}
		}
		if(blackCount==0){
			myText.setText("White Wins!");
		}
		if(whiteCount==0){
			myText.setText("Black Wins!");
		}
		if(blackCount+whiteCount==64){
			if(blackCount<whiteCount){
				myText.setText("White Wins!");
			}
			if(whiteCount<blackCount){
				myText.setText("Black Wins!");
			}
			if(whiteCount==32 && blackCount==32){
				myText.setText("Draw!");
			}}}
	/**
	 * this method return the turn
	 */
	public Turn getTurn(){
		return turn;
	}
	/**
	 * this method repaints all the squares of the game board gray	
	 */
	public void repaintSquares(){
		Color color = Color.gray;
		for(int r=0; r<8; r++){
			for(int c=0; c<8; c++){
			panel.squares[r][c].changeColor(color);
			}}}
	/**
	 * this method flips the disks of between the new disk and an adjacent disk
	 * when a valid move is made
	 */
	public void flip(Turn thisTurn){
		//System.out.println("Number of disks flipped: " + disksToFlip.size());
		int tempR =0;
		int tempC =0;
		for(int i=0; i<disksToFlip.size(); i++){
			tempR=disksToFlip.get(i).getRow();
			tempC=disksToFlip.get(i).getColumn();
			panel.disks[tempR][tempC].changeColor(thisTurn.getColor());
			repaint();
		}
		//disksToFlip=new ArrayList<>();
	}
	/**
	 * this method determines if the mouse is hovering over a square
	 * that is a legal move for that particular user
	 */
	public boolean isValidMove(Square squareOn){
		boolean rVal = false;
		if(squareEmpty(squareOn)==false){
			return false;
		}
		
		//check if the square hovered over has a disk immediately next to it in any direction
		if(hasDiskToTheRight(turn, squareOn) || hasDiskBelow(turn,squareOn) 
				|| hasDiskToTheLeft(turn, squareOn) || hasDiskAbove(turn, squareOn) 
				|| hasDiskDiagonalUpLeft(turn, squareOn) || hasDiskDiagonalDownRight(turn, squareOn) 
				|| hasDiskDiagonalDownLeft(turn, squareOn) || hasDiskDiagonalUpRight(turn, squareOn)){
			rVal= true;
		}
		
		//flip all the disks, if the current valid square is clicked
		if(clicked==true){
			flip(turn);
		}
		return rVal;
	}
	/**
	 * determines if square hovered over has a disk to the right that need to be flipped
	 */
	public  boolean hasDiskToTheRight(Turn turn, Square squareOn){
		int row = squareOn.getRow(); //keeps the square hovered on row
		int column = squareOn.getColumn(); //keeps the square hovered on column
		Color clr = turn.getColor();
		int diskFound = -1;
		boolean rVal = false;
		if(column==7){ //column 7 can't have a disk to the right of it
			return false;
		}
		for(int c=column; c<8; c++){ //goes through the row to determine if a disk of the same color exist
			if(panel.disks[row][c]!=null && panel.disks[row][c].getColor()==clr){
				diskFound=c; //saves the column of the next disk in the same row
				rVal=true; 
				break;
			}
		}
		if(diskFound==-1){//no disk was found to the right
			return false;
		}
		if(diskFound==column+1){//the disk found can't be immediately next to the squareOn
			return false;
		}
		
		for(int c=column+1; c<diskFound; c++){ //checks the disk between squareOn and diskFound
			if(panel.disks[row][c]==null || panel.squares[row][c].getState()==State.EMPTY){ //cant have an empty square between squareOn and diskFound
				return false;
			}
			if(panel.disks[row][c].getColor()==clr){ //middle disks cant be same color as disk found
				return false;								
			}
		}
		for(int c=column+1; c<diskFound; c++){//adds the disks that need to be flipped
			if(clicked==true){
				disksToFlip.add(panel.disks[row][c]);
			}
		}
		return rVal;
	}
	/**
	 * determines if the square hovered over has a disk to the left that need to be flipped
	 */
	public boolean hasDiskToTheLeft(Turn turn, Square squareOn){
		int row = squareOn.getRow(); //keeps the square hovered on row
		int column= squareOn.getColumn(); //keeps the square hovered on column
		Color clr = turn.getColor();
		int diskFound = -1;
		boolean rVal = false;
		if(column==0){ //column 0 can't have a disk to the left of it
			return false;
		}
		for(int c=column; c>-1; c--){ //goes through the row to determine if a disk of the same color exist
			if(panel.disks[row][c]!=null && panel.disks[row][c].getColor()==clr){ 
				diskFound=c;//saves the column of the disk found
				rVal= true;
				break;
			}
		}
		if(diskFound==-1){//no disk was found left of squareOn
			return false;
		}
		if(diskFound==column-1){//disk can't be immediately next to squareOn
			return false;	
		}
		for(int c=column-1; c>diskFound; c--){ //checks the disks between squareOn and diskFound
			if(panel.disks[row][c]==null){ //can't have an empty square between squareOn and diskFound
				return false;
			}
			if(panel.squares[row][c].getState()==State.EMPTY){
				return false;
			}
			if(panel.disks[row][c].getColor()==clr){ //no middle disk can have same color as diskFound
				return false;
			}
		}
		for(int c=column-1; c>diskFound; c--){ // adds the disks that need to be flipped
			if(clicked==true){
				disksToFlip.add(panel.disks[row][c]);
			}
		}
		return rVal;
	}
	/**
	 * determines if the square hovered over has disks below that need to be flipped
	 */
	public boolean hasDiskBelow(Turn turn, Square squareOn){
		int row = squareOn.getRow(); //keeps the row of the square hovered over
		int column= squareOn.getColumn(); //keeps the column of the square hovered over
		Color clr = turn.getColor();
		int diskFound = -1;
		boolean rVal = false;
		if(row==7){
			return false;
		}
		for(int r=row; r<8; r++){ //goes through the column to determine if a disk of the same color exist
			if(panel.disks[r][column]!=null && panel.disks[r][column].getColor()==clr){ 
				diskFound=r;//saves the column of the disk found
				rVal= true;
				break;
			}
		}
		if(diskFound==-1){//no disk was found below squareOn
			return false;
		}
		if(diskFound==row+1){//disk can't be immediately below squareOn
			return false;	
		}
		for(int r=row+1; r<diskFound; r++){ //checks the disks between squareOn and diskFound
			if(panel.disks[r][column]==null || panel.squares[r][column].getState()==State.EMPTY){ 
				return false; //can't have an empty square between squareOn and diskFound
			}
			if(panel.disks[r][column].getColor()==clr){ //no middle disk can have same color as diskFound
				return false;
			}
		}
		for(int r=row+1; r<diskFound; r++){ //adds the disks that need to be flipped
			if(clicked==true){
				disksToFlip.add(panel.disks[r][column]);
			}
		}		
		return rVal;
	}
	/**
	 * determines if the square hovered over has a disk above it that need to be flipped
	 */
	public boolean hasDiskAbove(Turn turn, Square squareOn){
		int row = squareOn.getRow(); //keeps the row of the square hovered over
		int column= squareOn.getColumn(); //keeps the column of the square hovered over
		Color clr = turn.getColor();
		int diskFound = -1;
		boolean rVal = false;
		if(row==0){
			return false;
		}
		for(int r=row; r>=0; r--){ //goes through the column to determine if a disk of the same color exist
			if(panel.disks[r][column]!=null && panel.disks[r][column].getColor()==clr){ 
				diskFound=r;//saves the column of the disk found
				rVal= true;
				break;
			}
		}
		if(diskFound==-1){//no disk was found above squareOn
			return false;
		}
		if(diskFound==row-1){//disk can't be immediately above squareOn
			return false;	
		}
		for(int r=row-1; r>diskFound; r--){ //checks the disks between squareOn and diskFound
			if(panel.disks[r][column]==null|| panel.squares[r][column].getState()==State.EMPTY){ 
				return false; //can't have an empty square between squareOn and diskFound
			}
			if(panel.disks[r][column].getColor()==clr){ //no middle disk can have same color as diskFound
				return false;
			}
		}
		for(int r=row-1; r>diskFound; r--){ //adds the disks that need to be flipped
			if(clicked==true){
				disksToFlip.add(panel.disks[r][column]);
			}
		}
		return rVal;
	}
	/**
	 * this method determines if the square hovered over has disks diagonal up to the left that need to be flipped
	 */
	public boolean hasDiskDiagonalUpLeft(Turn turn, Square squareOn){
		int row = squareOn.getRow(); //keeps the row of the square hovered over
		int column= squareOn.getColumn(); //keeps the column of the square hovered over
		Color clr = turn.getColor();
		int diskFoundColumn = -1;
		int diskFoundRow = -1;
		boolean rVal = false;
		if(row<2 || column<2){
			return false;
		}
		for(int adder=1; row-adder>-1 && column-adder>-1; adder++){ //goes through the diagonal to find a disk of the same color
				if(panel.disks[row-adder][column-adder]!=null && panel.disks[row-adder][column-adder].getColor()==clr){ 
					diskFoundRow=row-adder;//saves the row of the disk found
					diskFoundColumn=column-adder;//saves the column of the disk found
					rVal= true;
					break;
				}
		}
		if(diskFoundColumn ==-1){ //no disks were found
			return false;
		}
		if(diskFoundColumn == column-1 || diskFoundRow == row-1){ //disk found can't be immediately next to the square hovered over
			return false;
		}
		for(int adder=1; row-adder>diskFoundRow && column-adder>diskFoundColumn; adder++){
			if(panel.disks[row-adder][column-adder]==null|| panel.squares[row-adder][column-adder].getState()==State.EMPTY){ 
				return false; //can't have an empty square between squareOn and diskFound
			}
			if(panel.disks[row-adder][column-adder].getColor()==clr){ //no middle disk can have same color as diskFound
				return false;
			}
		}
		for(int adder=1; row-adder>diskFoundRow && column-adder>diskFoundColumn; adder++){
			if(clicked==true){
				disksToFlip.add(panel.disks[row-adder][column-adder]);
			}
		}
		return rVal;
	}
	/**
	 * this method determines if the square hovered over has any disks diagonal down to the right that need to be flipped
	 */
	public boolean hasDiskDiagonalDownRight(Turn turn, Square squareOn){
		int row = squareOn.getRow();
		int column= squareOn.getColumn();
		Color clr = turn.getColor();
		int diskFoundColumn = -1;
		int diskFoundRow = -1;
		boolean rVal = false;
		if(row>5 || column>5){
			return false;
		}
		for(int adder=1; row+adder<8 && column+adder<8; adder++){ //goes through the diagonal down right
			if(panel.disks[row+adder][column+adder]!=null && panel.disks[row+adder][column+adder].getColor()==clr){
				diskFoundColumn=column+adder;//saves the column of the disk found
				diskFoundRow=row+adder;//saves the row of the disk found
				rVal=true;
				break;
			}
		}
		if(diskFoundColumn ==-1){
			return false;
		}
		if(diskFoundColumn == column+1 || diskFoundRow == row+1){
			return false;
		}
		for(int adder=1; row+adder<diskFoundRow && column+adder<diskFoundColumn; adder++){
			if(panel.disks[row+adder][column+adder]==null|| panel.squares[row+adder][column+adder].getState()==State.EMPTY){ //can't have an empty square between squareOn and diskFound
				return false;
			}
			if(panel.disks[row+adder][column+adder].getColor()==clr){ //no middle disk can have same color as diskFound
				return false;
			}
		}
		for(int adder=1; row+adder<diskFoundRow && column+adder<diskFoundColumn; adder++){
			if(clicked==true){
				disksToFlip.add(panel.disks[row+adder][column+adder]);
			}
		}
		return rVal;
	}
	/**
	 * this method determines if the square hovered over has any disks diagonal down to the left that need to be flipped
	 */
	public boolean hasDiskDiagonalDownLeft(Turn turn, Square squareOn){
		int row = squareOn.getRow();
		int column= squareOn.getColumn();
		Color clr = turn.getColor();
		int diskFoundColumn = -1;
		int diskFoundRow = -1;
		boolean rVal = false;
		if(row>5 || column<2){
			return false;
		}
		for(int adder=1; row+adder<8 && column-adder>-1; adder++){ //goes through the diagonal down right
			if(panel.disks[row+adder][column-adder]!=null && panel.disks[row+adder][column-adder].getColor()==clr){
				diskFoundColumn=column-adder;//saves the column of the disk found
				diskFoundRow=row+adder;//saves the row of the disk found
				rVal=true;
				break;
			}
		}
		if(diskFoundColumn ==-1){
			return false;
		}
		if(diskFoundColumn == column-1 || diskFoundRow == row+1){
			return false;
		}
		for(int adder=1; row+adder<diskFoundRow && column-adder>diskFoundColumn; adder++){
			if(panel.disks[row+adder][column-adder]==null){ //can't have an empty square between squareOn and diskFound
				return false;
			}
			if(panel.disks[row+adder][column-adder].getColor()==clr){ //no middle disk can have same color as diskFound
				return false;
			}
		}
		for(int adder=1; row+adder<diskFoundRow && column-adder>diskFoundColumn; adder++){
			if(clicked==true){ //adds the disks that need to be flipped
				disksToFlip.add(panel.disks[row+adder][column-adder]);
			}
		}
		return rVal;
	}
	/**
	 * this method determines if the square hovered over has any disks diagonal up to the right that need to be flipped
	 */
	public boolean hasDiskDiagonalUpRight(Turn turn, Square squareOn){
		int row = squareOn.getRow(); //keeps the row of the square hovered over 
		int column= squareOn.getColumn(); //keeps the column of the square hovered over
		Color clr = turn.getColor();
		int diskFoundColumn = -1;
		int diskFoundRow = -1;
		boolean rVal = false;
		if(row<2 || column>5){
			return false;
		}
		for(int adder=1; row-adder>-1 && column+adder<8; adder++){ //goes through the diagonal down right
			if(panel.disks[row-adder][column+adder]!=null && panel.disks[row-adder][column+adder].getColor()==clr){
				diskFoundColumn=column+adder;//saves the column of the disk found
				diskFoundRow=row-adder;//saves the row of the disk found
				rVal=true;
				break;
			}
		}
		if(diskFoundColumn ==-1){
			return false;
		}
		if(diskFoundColumn == column+1 || diskFoundRow == row-1){
			return false;
		}
		for(int adder=1; row-adder>diskFoundRow && column+adder<diskFoundColumn; adder++){
			if(panel.disks[row-adder][column+adder]==null|| panel.squares[row-adder][column+adder].getState()==State.EMPTY){ //can't have an empty square between squareOn and diskFound
				return false;
			}
			if(panel.disks[row-adder][column+adder].getColor()==clr){ //no middle disk can have same color as diskFound
				return false;
			}
		}
		for(int adder=1; row-adder>diskFoundRow && column+adder<diskFoundColumn; adder++){
			if(clicked==true){ //adds the disks that need to be flipped
				disksToFlip.add(panel.disks[row-adder][column+adder]);
			}
		}
		return rVal;
	}
	/**
	 * this class delays the computers move 
	 */
	public class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			changeTurn(turn);
			t.stop();
		}
	}
	/**
	 * this class listens for when the new game menu item is pressed
	 */
	public class newGameListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			boardClear();
		}
	}
	/**
	 * this classlistens for when the exit menu is pressed
	 */
	public class exitListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.exit(0);
		}
		
	}
	/**
	 * this method clears the board
	 */
	public void boardClear(){
		repaintSquares();
		panel.squares=new Square[8][8];
		panel.disks=new Disk[8][8];
		panel.initialPieces();
		clicked = false;
		myText.setText("");
		whiteCount=2;
		blackCount=2;
		legalComputerMoves=new ArrayList<>();
		}
}