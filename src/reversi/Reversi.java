package reversi;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JFrame;

/**
 * This main class is used to open the frame
 * @author Germaine
 *
 */
public class Reversi{
	// declare variables
	//private final ArrayList<Square> squares;
	private Color color;
	/**
	 * this main method continuously allows the user/computer to make a
	 * @param args
	 */
	public static void main(String[] args) {
		//open the frame
		MyFrame frame = new MyFrame();
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setTitle("Reversi Game");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}