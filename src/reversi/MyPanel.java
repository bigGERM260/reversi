package reversi;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.util.*;

import javax.swing.*;

/**
 * this enum is stored in each square to determine if the square
 * contains a disk or not
 */
enum State{OCCUPIED, EMPTY;
	
}
/**
 * this enum is also stored in each square to store weather that
 * square is a valid move
 */
enum Valid{NOTVALID, VALID;

}

/**
 * this class does all of the drawing for the program
 * 
 * @author Germaine
 *
 */
public class MyPanel extends JPanel{
	// declare variables
	public Square[][] squares = new Square[8][8];
	public Disk[][] disks = new Disk[8][8];
	private Color color;
	private int row;
	private int column;
	private Graphics2D g2;
	/**
	 * this is the constructor for the MyPanel class
	 */
	public MyPanel(){
		initialPieces(); //colors in the initial peices
	}
	
	/**
	 * this method does all of the painting for the program
	 */
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g2 = (Graphics2D) g;
		//squares[1][1].draw(g2);
		for (int r=0; r<8; r++){
			for (int c=0; c<8; c++){
				squares[r][c].draw(g2);
				if (disks[r][c]!=null){
					disks[r][c].draw(g2);
				}
			}
		}
	}
	/**
	 * this method returns the squares array
	 */
	public Square[][] getSquares(){
		return squares;
	}
	/**
	 * this method returns the disks array
	 */
	public Disk[][] getDisks(){
		return disks;
	}
	/**
	 * this method draws the starting disks and game board
	 */
	public void initialPieces(){
		// draw the game board
		for(row=0; row<8; row++){
			for(column=0; column<8; column++){
				squares[row][column]=(new Square(row, column, State.EMPTY,Valid.NOTVALID));
			}
		}
		// draw the initial player/computer pieces
		disks[3][3]=(new Disk(3,3, Turn.COMPUTER.getColor())); //3,3
		disks[4][4]=(new Disk(4,4, Turn.COMPUTER.getColor())); //4,4
		disks[3][4]=(new Disk(3,4, Turn.PERSON.getColor()));	//3,4
		disks[4][3]=(new Disk(4,3, Turn.PERSON.getColor()));	//4,3
		// change state of initial squares
		squares[3][3].changeState();
		squares[4][4].changeState();
		squares[3][4].changeState();
		squares[4][3].changeState();	
	}
	/**
	 * this method displays the game over message
	 */
	public void showMessage(String text){
		//create font style
		Font myFont = new Font("SanSerif", Font.BOLD, 40);
		//set g2 font to myFont
		g2.setFont(myFont);
		g2.setColor(Color.red);
		//create text box
		Rectangle2D textBox = myFont.getStringBounds(text, g2.getFontRenderContext());
		g2.drawString(text, (int)(getWidth()/2-textBox.getWidth()/2),
				(int)(getHeight()/2));}
	/**
	 * this method add a new disk
	 */
	public void addDisk(int row, int column, Turn turn){
		disks[row][column]=(new Disk(row, column, turn.getColor()));
		repaint();
	}
}