package reversi;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D.Double;

/**
 * this class creates the disks for the player/computer
 * @author Germaine
 *
 */
public class Disk {
	// declare variables
	private final int gap = 2;
	private int row;
	private int column;
	private int xLocation;
	private int yLocation;
	private final int size = 50;
	private Color color;
	private Turn turn;
	/**
	 * this is the constructor for the disks it sets each disk
	 * location and hold the player it belongs to and the color
	 */
	public Disk(int row, int column, Color color){
		this.row = row;
		this.column = column;
		this.color = color;
		xLocation = (size*column)+(gap*(column+1));
		yLocation = (size*row)+(gap*(row+1));
		// this.player=player;
	}
	/**
	 * this method draws the disk
	 */
	public void draw(Graphics2D g2){
		g2.setPaint(color);
		g2.fill(new Ellipse2D.Double(xLocation,yLocation,size,size));
	}
	/**
	 * this method returns the row
	 */
	public int getRow(){
		return row;
	}
	/**
	 * this method return the column
	 */
	public int getColumn(){
		return column;
	}
	/**
	 * this method return the color
	 */
	public Color getColor(){
		return color;
	}
	/**
	 * this method changes the color of the disk
	 */
	public void changeColor(Color color){
		this.color = color;
	}
}